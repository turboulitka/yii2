<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 */
class m200330_180356_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'user_id' => $this->smallInteger(8),
            'title' => $this->string(20),
            'value' => $this->integer(10),
            'date_start' => $this->timestamp(),
            'date_end' => $this->timestamp(),
        ]);

        $this->alterColumn('projects', 'id', $this->smallInteger(8). 'NOT NULL AUTO_INCREMENT');

        $this->addForeignKey(
            'users_to_projects',
            'projects',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%projects}}');
    }
}
