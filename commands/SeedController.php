<?php


namespace app\commands;
use yii\console\Controller;

use app\models\User;

class SeedController extends Controller
{
    public function actionIndex()
    {
        $user = new User();

        $user->setIsNewRecord(true);
        $user->username = 'admin';
        $user->FIO = 'admin';
        $user->password = 'admin';
        $user->generateAuthKey();
        $user->save();
    }
}